const div_ferramentas=document.getElementById("ferramentas");
const div_paleta=document.getElementById("paleta");
const div_cor1=document.getElementById("cor1");
const div_cor2=document.getElementById("cor2");
const div_cores=document.getElementById("cores");
const div_container=document.getElementById("container");
const canvas_tela=document.getElementById("tela");
const input_zoom=document.getElementById("zoom");
const input_tamanhoX=document.getElementById("tamanhoX");
const input_tamanhoY=document.getElementById("tamanhoY");
const div_telaLimits=document.getElementById("telaLimits");
const span_posicaoXY=document.getElementById("posicaoXY");
var contextCanvas=canvas_tela.getContext("2d");
canvas_tela.onmousedown=acaoTelaDown;
canvas_tela.onmousemove=acaoTelaMove;
//canvas_tela.onmouseup=acaoTelaUp;
var telaX=1;
var telaY=1;
var zoom=0;
var cor1="000000";
var cor2="ffffff";
var mousePintando=false;
var arrastouMouse=false;
var ferramenta="lapis";

//Funções de inicialização
function iniciarTela(argX,argY) {
	input_tamanhoX.value=argX;
	input_tamanhoY.value=argY;
	canvas_tela.setAttribute("width",telaX);
	canvas_tela.setAttribute("height",telaY);
	definirTamanho(argX,argY);
	definirZoom(2000);
}
function iniciarPaleta(argProfundidade) {
	var novasCores=document.createElement("div");
	for (var i = 0; i<256; i+=(255/argProfundidade)) {
		var novaCor=document.createElement("div");
		var corGerada="";
		var corGerada=(Math.round(i)).toString(16);
		if (corGerada.length==1) {
			corGerada="0"+corGerada;
		}
		corGerada+=corGerada+corGerada;
		novaCor.setAttribute("onmousedown","definirCor(\""+corGerada+"\")");
		novaCor.setAttribute("oncontextmenu","event.preventDefault()");
		novaCor.id="cor"+corGerada;
		novaCor.style.backgroundColor="#"+corGerada;
		novasCores.appendChild(novaCor);
	}
	for (var r = 0; r<256; r+=(255/argProfundidade)) {
		for (var g = 0; g<256; g+=(255/argProfundidade)) {
			for (var b = 0; b<256; b+=(255/argProfundidade)) {
				var novaCor=document.createElement("div");
				var corGerada="";
				var corGeradaR=(Math.round(r)).toString(16);
				if (corGeradaR.length==1) {
					corGeradaR="0"+corGeradaR;
				}
				corGerada+=corGeradaR;
				var corGeradaG=(Math.round(g)).toString(16);
				if (corGeradaG.length==1) {
					corGeradaG="0"+corGeradaG;
				}
				corGerada+=corGeradaG;
				var corGeradaB=(Math.round(b)).toString(16);
				if (corGeradaB.length==1) {
					corGeradaB="0"+corGeradaB;
				}
				corGerada+=corGeradaB;
				var corOk=true;
				novasCores.childNodes.forEach(corAdicionada=>{
					if (corAdicionada.id=="cor"+corGerada) {
						corOk=false;
					}
				});
				if (corOk) {
					novaCor.id="cor"+corGerada;
					novaCor.title="#"+corGerada;
					novaCor.setAttribute("onmousedown","definirCor(\""+corGerada+"\")");
					novaCor.setAttribute("oncontextmenu","event.preventDefault()");
					novaCor.style.backgroundColor="#"+corGerada;
					novasCores.appendChild(novaCor);
				}
			}
		}
	}
	div_cores.innerHTML=novasCores.innerHTML;
}
function iniciarLGPaint() {
	iniciarPaleta(4);
	resetarCores();
	selecionarFerramenta("lapis");
	iniciarTela(32,32);
}

//Funções do editor
function definirCor(argCor) {
	if (event.button==0) {
		definirCor1(argCor);
	}
	if (event.button==2) {
		definirCor2(argCor);
	}
}
function definirCor1(argCor) {
	document.getElementById("cor"+cor1).classList.remove("selecionado");
	cor1=argCor;
	document.getElementById("cor"+cor1).classList.add("selecionado");
	div_cor1.style.backgroundColor="#"+cor1;
}
function definirCor2(argCor) {
	document.getElementById("cor"+cor2).classList.remove("selecionado");
	cor2=argCor;
	document.getElementById("cor"+cor2).classList.add("selecionado");
	div_cor2.style.backgroundColor="#"+cor2;
}
function inverterCores() {
	var corInvertida=cor1;
	cor1=cor2;
	cor2=corInvertida;
	div_cor1.style.backgroundColor=cor1;
	div_cor2.style.backgroundColor=cor2;
}
function resetarCores() {
	definirCor1("000000");
	definirCor2("ffffff");
}
function definirZoom(argZoom=1) {
	var limiteMenorZoom=0.5;
	var limiteMaiorZoom=50;
	if (argZoom=="-") {
		if (zoom>limiteMenorZoom) {
			argZoom=zoom-1;
			zoom=argZoom;
		}
	} else if (argZoom=="+") {
		if (zoom<limiteMaiorZoom) {
			argZoom=zoom+1;
			zoom=argZoom;
		}
	} else {
		zoom=argZoom/100;
	}
	if (zoom<limiteMenorZoom) {
		zoom=limiteMenorZoom;
	}
	if (zoom>limiteMaiorZoom) {
		zoom=limiteMaiorZoom;
	}
	input_zoom.value=zoom*100;
	canvas_tela.style.transform="scale("+zoom+")";
	atualizarLimitesTela();
}
function definirTamanho(argX,argY) {
	telaX=argX;
	telaY=argY;
	var canvasTmp=document.createElement("canvas");
	var contextCanvasTmp=canvasTmp.getContext("2d");
	canvasTmp.setAttribute("width",telaX);
	canvasTmp.setAttribute("height",telaY);
	contextCanvasTmp.drawImage(canvas_tela,0,0);
	canvas_tela.setAttribute("width",telaX);
	canvas_tela.setAttribute("height",telaY);
	contextCanvas.drawImage(canvasTmp,0,0);
	atualizarLimitesTela();
}
function definirTamanhoX(argX) {
	definirTamanho(argX,telaY);
}
function definirTamanhoY(argY) {
	definirTamanho(telaX,argY);
}
function selecionarFerramenta(argFerramenta) {
	document.getElementById("ferramenta_"+ferramenta).classList.remove("selecionado");
	ferramenta=argFerramenta;
	document.getElementById("ferramenta_"+ferramenta).classList.add("selecionado");
}
function atualizarLimitesTela() {
	div_telaLimits.style.width=canvas_tela.width*zoom;
	div_telaLimits.style.height=canvas_tela.height*zoom;
}
function atualizarPosicaoXY() {
	calculoX=Math.round((((event.clientX+div_container.scrollLeft)-canvas_tela.offsetLeft-canvas_tela.offsetParent.offsetLeft)/zoom)-0.5);
	calculoY=Math.round((((event.clientY+div_container.scrollTop)-canvas_tela.offsetTop-canvas_tela.offsetParent.offsetTop)/zoom)-0.5);
	span_posicaoXY.innerHTML="X: "+calculoX+" | Y: "+calculoY;
}

//Funções de pintura
var mouseXAnterior=0;
var mouseYAnterior=0;
function acaoTelaDown() {
	if (!mousePintando) {
		contextCanvas=canvas_tela.getContext("2d");
		arrastouMouse=false;
		mousePintando=true;
		calculoX=Math.round((((event.clientX+div_container.scrollLeft)-canvas_tela.offsetLeft-canvas_tela.offsetParent.offsetLeft)/zoom)-0.5);
		calculoY=Math.round((((event.clientY+div_container.scrollTop)-canvas_tela.offsetTop-canvas_tela.offsetParent.offsetTop)/zoom)-0.5);
		switch (ferramenta) {
			case "lapis": {
				if (event.button==0) {
					contextCanvas.fillStyle = "#"+cor1;
				}
				if (event.button==2) {
					contextCanvas.fillStyle = "#"+cor2;
				}
				contextCanvas.fillRect(calculoX,calculoY,1,1);
			} break;
			case "borracha": {
				contextCanvas.clearRect(calculoX,calculoY,1,1);
			}
		}
		mouseXAnterior=event.offsetX;
		mouseYAnterior=event.offsetY;
	}
}
function acaoTelaMove() {
	if (mousePintando) {
		arrastouMouse=true;
		calculoX=Math.round((((event.clientX+div_container.scrollLeft)-canvas_tela.offsetLeft-canvas_tela.offsetParent.offsetLeft)/zoom)-0.5);
		calculoY=Math.round((((event.clientY+div_container.scrollTop)-canvas_tela.offsetTop-canvas_tela.offsetParent.offsetTop)/zoom)-0.5);
		switch (ferramenta) {
			case "lapis": {
				contextCanvas.fillRect(calculoX,calculoY,1,1);
			} break;
			case "borracha": {
				contextCanvas.clearRect(calculoX,calculoY,1,1);
			}
		}
		mouseXAnterior=event.offsetX;
		mouseYAnterior=event.offsetY;
	}
	atualizarPosicaoXY();
}
function acaoTelaUp() {
	if (mousePintando) {
		mousePintando=false;
	}
}

//Event listeners:
document.addEventListener("contextmenu",function(event){
	if (arrastouMouse) {
		event.preventDefault();
	}
});
document.addEventListener("mouseup",function(event){
	acaoTelaUp();
});

//Gerenciamento de arquivos:
function salvarImagem() {
	var arquivoImagem=canvas_tela.toDataURL("image/png", 1.0).replace(/^data:image\/png/,'data:application/octet-stream');
	var linkArquivo = document.createElement("a");
	linkArquivo.download = "imagem.png";
	linkArquivo.href = arquivoImagem;
	linkArquivo.click();
}
function abrirImagem() {
	var inputImagem = document.createElement("input");
	inputImagem.type="file";
	inputImagem.onchange = function() {
		var arquivoImagem = new FileReader();
		arquivoImagem.onload = function(event) {
			var imagem = new Image();
			imagem.onload = function(){
				definirTamanho(imagem.width,imagem.height);
				contextCanvas.clearRect(0,0,telaX,telaY);
				contextCanvas.drawImage(imagem,0,0);
			}
			imagem.src = event.target.result;
		}
		arquivoImagem.readAsDataURL(event.target.files[0]);
	}
	inputImagem.click();
}

//Iiiiiiiiiiiiiiiiiit's SHOWTIME!
iniciarLGPaint();